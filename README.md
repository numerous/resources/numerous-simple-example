# A simple example to get started using the `numerous` platform

## Introduction

This repository contains a simple example of how to code a model for the
`numerous` platform. It explains how to use the `numerous` command-line tool to
upload your code to the platform, and how to use the python package
`numerous.image_tools` to connect your code with the platform.

The model itself is very simple. It is called `multiply_by_two` and as the name
implies, it takes an input and multiplies it by 2, after which the output is
saved to numerous. You can replace the example code, with your own code, and use
this example as a starting point for more advanced stuff.

- [A simple example to get started using the `numerous` platform](#a-simple-example-to-get-started-using-the-numerous-platform)
  - [Introduction](#introduction)
  - [Requirements](#requirements)
  - [Not covered by this guide](#not-covered-by-this-guide)
  - [Getting started](#getting-started)
    - [Step 1. Clone](#step-1-clone)
    - [Step 2. Install dependencies](#step-2-install-dependencies)
    - [Step 3. Create a new system](#step-3-create-a-new-system)
    - [Step 4. Create scenario from the system](#step-4-create-scenario-from-the-system)
    - [Step 5. Connect the command line to your scenario](#step-5-connect-the-command-line-to-your-scenario)
    - [Step 6. Push code to numerous repository, and build it](#step-6-push-code-to-numerous-repository-and-build-it)
    - [Step 7. Run the code, and plot the data in numerous](#step-7-run-the-code-and-plot-the-data-in-numerous)
    - [Step 8. Share with coworkers](#step-8-share-with-coworkers)
  - [Congratulations](#congratulations)

## Requirements

To run the code, it is assumed you have access to a numerous environment, and
that you're somewhat familiar with python.

More specific requirements are:
- Python version >= 3.9
- Installation of `numerous-image-tools` version >= 2.14.4 (done automatically
  through `pip install -r requirements.txt`)
- Installation of `numerous-cli` (done automatically as a dependency of
  `numerous-image-tools`)

## Not covered by this guide

This guide does not cover how to write python code, and does not cover
extensively, all the documentation of the code inside `multiply_by_two_job.py`.

## Getting started

In the following the 7 steps are described in more detail, which is what you
will need in order to build your first numerous model.

### Step 1. Clone

You can clone either with the [git](https://git-scm.com/) command line tool, or
in most IDEs.

In your IDE you should be able to clone the repository using the following url:

    https://gitlab.com/numerous/resources/numerous-simple-example.git

And if you use the git command line, you can run the command:

    git clone https://gitlab.com/numerous/resources/numerous-simple-example.git


### Step 2. Install dependencies

💡 *We recommend that you create a
[virtual environment](https://docs.python.org/3/tutorial/venv.html), and activate it
so you do not clutter your global python environment with the dependencies for this
project.*

And inside the virtual environment - if you created it - run:

    pip install -r requirements.txt

This will install the package `numerous-image-tools`, that is required to run
this example.

### Step 3. Create a new system

💡 *In numerous a system is an abstract simulation system. It contains a
reference to the code that should run, and a specification of what parameters
and inputs should be used.*

To create a new system, open your numerous environment - eg.
[sandbox.numerous.cloud](https://sandbox.numerous.cloud) - and go to
[systems](https://sandbox.numerous.cloud/systems).

Click **Add new system**, as seen below, and give your system a name. It is
a good idea to give your system a unique name that you can find later.

![image](guide/images/new_system.png)

💡 *In numerous a **job** is how you run code on the platform. They reference
the code that run, and in scenarios they can be started with a click.*

Next, we'll need to modify the system to fit our code. First, we need to add a
"job", so click the **Add job** button

![image](guide/images/add_job.png)

This opens the job page, where you can simply leave the values as they are and
click **Add Job**, which will take you back to the "System page".

![image](guide/images/new_job.png)

💡 *In numerous a component is a way to enable users of the simulations to
configure them by specifying parameters, selecting inputs, and even adding
sub-components.*

Next we need to add a component to the system. To do this we click the
**Add Component Type** button,

![image](guide/images/add_component.png)

and select *"Blank component"* from the component type selector:

![image](guide/images/add_component_selector.png)

Then give your component a *"Display Name"* and a *"Name/ID"* like this,

![image](guide/images/add_new_component.png)

and click the **Add To System** button. The chosen *"Name/ID"*  value `example`,
is the one we can use later when referring to this component inside the python
code.

Now you can see the component inside the system. You open the component, and
configure more things if you click the button marked here:

![image](guide/images/system_component.png)

This will bring up the component view, and now you can do the following steps:

1. Set the *"Item class"* value for the Example Component to
   `src.multiply_by_two.entrypoint`. This means that the `entrypoint` function
   in the python module `src.multiply_by_two` is called for this component. This
   function should return the *model object* for the component.
2. Next, add a new *Input* , by clicking the **+** button under the
   **Input variables** section.
3. Now fill out the fields of the input you added. Set the **ID** to
4. Finally, click **Save system changes** that is displayed at the very bottom
   of the screen after making changes to the system.

![image](guide/images/setup_system_item_class_and_input.png)

### Step 4. Create scenario from the system

💡 *In numerous, all simulation **scenarios** are organized in **projects** and
**groups**. A project can contain multiple groups, which in turn can contain
multiple scenarios.*

If you already have a project and group you can open this immediately. If you do
not, then you will need to create these.

Don't worry, it's easy! Just click the **Projects** menu item on the left, and
then click the **Add New Project** button.

![image](guide/images/add_project.png)

And now you can give your project a cool name, like so, and click the
**Add Project** button, which will open the project page for your new project.

Next we create a new group inside the created project, by clicking the
**Add Group** button inside your new project.

![image](guide/images/add_group.png)

Give that a cool name and click the **Save** button to save the group.

![image](guide/images/new_group.png)

You can now add scenarios to this group by selecting the **Add Scenario**
button:

![image](guide/images/add_scenario.png)

This opens the scenario creation menu. Here you can give the scenario a cool
name, and select the system you created earlier.

![image](guide/images/new_scenario.png)

Now the scenario page should have been opened, and you can see an empty
scenario.

You can open the component to set the input source from `static` to `dynamic`,
if you have added an input scenario, but in this example, the input is static
and equal to 1.

![image](guide/images/my_cool_scenario.png)


🎉 **Congratulations** you finished the frontend configuration! Now, get out
your favorite python IDE and open the cloned project.

### Step 5. Connect the command line to your scenario

⚠️ *If you didn't already, make sure you install the `requirements.txt`,
preferably inside a virtual environment. Go back to
[Step 2. Install dependencies](#step-2-install-dependencies) if you need
a guide.*

Now we are going to use the numerous command line tool. It is a dependency of
the `numerous-image-tools`, so it should be installed if you followed the guide.

⚠️ *If you cannot use the `numerous` command in your terminal environment, you
need to make sure that the directory it has been installed in is a part of the
**PATH** environment variable.*

💡 *The numerous command line tool makes it possible to link code directories
to scenarios on the numerous platform. It does so by initializing a **numerous
repository**, which essentially means a directory that contains a special file
called `.numerous.repository.json`, containing information that it uses to
connecto to the platform.*

The easiest way to set-up your numerous repository is to follow the guide on the
website that is tailored to your scenario. You find it by first clicking the
▶️ run button to open the **run settings**.

![image](guide/images/open_run_settings.png)

From here you can click the **Edit job code** button to show a detailed guide
on how to

![image](guide/images/edit_job_code.png)

This opens the job code view, where you can find the following window that
contains the necessary commands to setup

![image](guide/images/job_code.png)

⚠️ *Here the capitalized placeholders such as REMOTE_ID will need to be replaced
with the correct values found in the **Setup new repository** guide.*

💡 *When using the numerous command line tool it will work on the current
working directory by default, but if a directory path is added as the last
argument, it will work in that directory instead, like in the
**Setup new repository** guide*

In your terminal, that is navigated to the directory that was cloned with git
run the following commands;

    numerous init

which will initialize the repository, and then configure the remote. Remember to
replace the `REMOTE_ID` with the value from the **Setup new repository** guide.

    numerous config --remote REMOTE_ID

Now you can *checkout* a scenario with. Remember to replace the `PROJECT_ID` and
`SCENARIO_ID` with the values from the **Setup new repository** guide.

    numerous checkout --scenario PROJECT_ID:SCENARIO_ID

### Step 6. Push code to numerous repository, and build it


⚠️ *The `.exclude` file in your numerous repository contains a list of file
names and paths to exclude from the build process. You should add the virtual
environment folder to this, eg. adding the line `venv` if your virtual
environment folder is called `venv`.*

With this is mind, simply run the **push** command, which uploads the current
version of the code and associates a comment with it:

    numerous push -c "initial commit"

After pushing completes, you can now perform the **build**, which will package
the code into a *Docker image*, and update the job in the scenario to run this
new version.

    numerous build

It may take a minute or so, but after a while the build should complete and you
can continue.

### Step 7. Run the code, and plot the data in numerous

Go to your scenario, click the ▶️ run button and **Start Simulation**. The code
should run to completion.

![image](guide/images/run_simulation.png)

You can create a plot by selecting the data output:

![image](guide/images/add_plot.png)

Add a new report:

![image](guide/images/add_report.png)

Give it a cool name, and click **Save**.

![image](guide/images/new_report.png)

Select the report.

![image](guide/images/new_figure.png)

Add a new line graph.

![image](guide/images/new_line_graph.png)

Choose the output tags by clicking the search field.

![image](guide/images/choose_output.png)

The output should now be shown.

![image](guide/images/output.png)

### Step 8. Share with coworkers

Now that you have built your code, and want to share it with your coworkers,
you can open the updated *"run settings"*, which contain a new **image path**
generated by the `numerous build` command. Refer back to the image in
[Step 6](#step-6-run-the-code-and-plot-the-data-in-numerous) to see how it
looked.

You can copy this image path, and insert it back into the job in the System, so
that future scenarios created from the system will include the new version of
the code.

Find your system page again - start with the **Systems** menu item to the left -
and edit the job, by clicking on the three dots **...** button.

![image](guide/images/edit_system_job.png)

You can now paste the image path under **Path**:

![image](guide/images/edited_job.png)

and remember to click **Confirm Edits**, and finally **Save system changes**:

![image](guide/images/updated_system.png)

Now when creating scenarios using this system, they will be using your newly
built model!

## Congratulations

🎉 You have completed this tutorial. Now go forth, and build even cooler models
to explore the power of `numerous`.
